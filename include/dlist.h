#ifndef DOUBLE_LIST_H
#define DOUBLE_LIST_H

#include <stdlib.h>
#include <stdbool.h>

#include "node.h"


#define VB_DLIST_SIZE 	sizeof(struct _dlist)

#define	VB_DL_NEXT 	0
#define VB_DL_PREV 	1


struct __vb_dlist {
    struct __vb_dnode head;
    struct __vb_dnode tail;
    size_t count;
};

typedef struct __dnode vb_dnode_t;
typedef struct __dlist vb_dlist_t;


vb_dlist_t *vb_dlist_create();
void vb_dlist_destroy(vb_dlist_t *l);

vb_dnode_t *vb_dlist_head(vb_dlist_t *l);
vb_dnode_t *vb_dlist_tail(vb_dlist_t *l);

bool vb_dlist_is_empty(vb_dlist_t *l);
bool vb_dlist_exist(vb_dlist_t *l);

bool vb_dlist_delete(vb_dlist_t *l, vb_dnode_t *node_cur);
void vb_dlist_add(vb_dlist_t *l, vb_dnode_t *node_cur, void *info);
void vb_dlist_add_head(vb_dlist_t *l, void *info);
void vb_dlist_add_tail(vb_dlist_t *l, void *info);

void vb_dlist_visit(vb_dnode_t *cur, bool dir, void (*visit)(void *));
void vb_dlist_visit_from_head(vb_dlist_t *l, void (*visit)(void *));
void vb_dlist_visit_from_tail(vb_dlist_t *l, void (*visit)(void *));

vb_dnode_t *vb_dlist_get_by_offset(vb_dlist_t *l, size_t pos, vb_dnode_t *n, bool dir);
vb_dnode_t *vb_dlist_get(vb_dlist_t *l, size_t pos);
vb_dnode_t *vb_dlist_rget(vb_dlist_t *l, size_t pos);


struct vb__dlist_namespace {
    vb_dlist_t *(*create)(void);
    void (*destroy)(vb_dlist_t *);

    vb_dnode_t *(*head)(vb_dlist_t *);
    vb_dnode_t *(*tail)(vb_dlist_t *);

    bool (*is_empty)(vb_dlist_t *);
    bool (*exist)(vb_dlist_t *);

    bool (*delete)(vb_dlist_t *, vb_dnode_t *);
    void (*add)(vb_dlist_t *, vb_dnode_t *, void *);
    void (*add_head)(vb_dlist_t *, void *);
    void (*add_tail)(vb_dlist_t *, void *);

    vb_dnode_t *(*get_by_offset)(vb_dlist_t *, size_t, vb_dnode_t *, bool);
    vb_dnode_t *(*get)(vb_dlist_t *, size_t);
    vb_dnode_t *(*rget)(vb_dlist_t *, size_t);

    void (*visit)(vb_dnode_t *, bool, void (*visit)(void *));    
    void (*visit_from_head)(vb_dlist_t *, void (*visit)(void *));
    void (*visit_from_tail)(vb_dlist_t *, void (*visit)(void *));
};


static struct vb_dlist_namespace
VB_Dlist = {
    .create           =  &vb_dlist_create,
    .destroy          =  &vb_dlist_destroy,
    .head             =  &vb_dlist_head,
    .tail             =  &vb_dlist_tail,
    .is_empty         =  &vb_dlist_is_empty,
    .exist            =  &vb_dlist_exist,
    .delete           =  &vb_dlist_delete,
    .add              =  &vb_dlist_add,
    .add_head         =  &vb_dlist_add_head,
    .add_tail         =  &vb_dlist_add_tail,
    .get_by_offset    =  &vb_dlist_get_by_offset,
    .get              =  &vb_dlist_get,
    .rget             =  &vb_dlist_rget,
    .visit            =  &vb_dlist_visit,
    .visit_from_head  =  &vb_dlist_visit_from_head,
    .visit_from_tail  =  &vb_dlist_visit_from_tail,
};

#endif
