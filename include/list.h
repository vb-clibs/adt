#ifndef LINEAR_LIST_H
#define LINEAR_LIST_H

#include <stdlib.h>
#include <stdbool.h>

#include "node.h"


#define NODE_SIZE 	sizeof(struct _node)


struct _list
{
	struct _node *head;
	size_t count;
};


typedef	struct _list* list_t;


node_t *list_head(list_t *l);
bool list_exist(list_t *l);
bool list_is_empty(list_t *l);

list_t *list_create();
void list_destroy(list_t *l);
bool list_delete(list_t *l, node_t *node_cur);
void list_visit(node_t *n, void (*visit)(void *));
void list_add(list_t *l, node_t *node_cur, void *info);

void list_cat(list_t *l, list_t *m);
list_t *list_slice(list_t *l, size_t n);
list_t *list_merge(list_t *l, list_t *m, int (*compare)(void *, void *));

node_t *list_search_index(list_t *l, size_t pos);
int list_search_item(list_t *l, void *item, int (*compare)(void *, void *));
void list_sort(list_t *l, void (*sort)(void *v, size_t n, int (*compare)(void *, void *)), int (*compare)(void *, void *));


struct _list_namespace
{
    node_t *(*list_head)(list_t *);
    bool (*list_no_exist)(list_t *);
    bool (*list_is_empty)(list_t *);

    list_t *(*list_create)();
    void (*list_destroy)(list_t *);
    bool (*list_delete)(list_t *, node_t *);
    void (*list_visit)(node_t *, void (*)(void *));
    void (*list_add)(list_t *, node_t *, void *);

    void (*list_cat)(list_t *, list_t *);
    list_t *(*list_slice)(list_t *, size_t);
    list_t *(*list_merge)(list_t *, list_t *, int (*)(void *, void *));

    node_t *(*list_search_index)(list_t *, size_t);
    int (*list_search_item)(list_t *, void *, int (*)(void *, void *));
    void (*list_sort)(list_t *, void (*)(void *, size_t, int (*)(void *, void *)), int (*)(void *, void *));
};

static const struct _list_namespace
List = {
    .head           = &list_head,
    .exist          = &list_exist,
    .is_empty       = &list_is_empty,
    .create         = &list_create,
    .destroy        = &list_destroy,
    .delete         = &list_delete,
    .visit          = &list_visit,
    .add            = &list_add,
    .cat            = &list_cat,
    .slice          = &list_slice,
    .merge          = &list_merge,
    .search_index   = &list_search_index,
    .search_item    = &list_search_item,
    .sort           = &list_sort,
};

#endif

