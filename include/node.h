#ifndef NODES_H
#define NODES_H


#define VB_SIZE_INFO  sizeof(void *)
#define VB_NODE_SIZE  sizeof(struct __node)
#define VB_DNODE_SIZE sizeof(struct __dnode)
#define VB_TNODE_SIZE sizeof(struct __tnode)


struct __node {
	void *info;
	struct __node *next;
};

struct __dnode {
	void *info;
	struct __dnode *next;
	struct __dnode *prev;
};

struct __tnode {
	void *info;
	struct __tnode *left;
	struct __tnode *center;
    struct __tnode *right;
};


typedef	struct __node* vb_node_t;
typedef	struct __dnode* vb_dnode_t;
typedef	struct __tnode* vb_tnode_t;

#endif
