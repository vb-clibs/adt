#ifndef DYNAMIC_VECTOR_H
#define DYNAMIC_VECTOR_H

#include <stdio.h>
#include <stdlib.h>


struct __vector {
    void **items;
    size_t len;
    size_t capacity;
};

typedef	struct __vector * vb_vector_t;


int vb_vector_len(vb_vector_t *);
void vb_vector_create(vb_vector_t *);
void vb_vector_free(vb_vector_t *);
void *vb_vector_get(vb_vector_t *, int);
void vb_vector_del(vb_vector_t *, int);
void vb_vector_add(vb_vector_t *, void *);
void vb_vector_set(vb_vector_t *, int, void *);
void vb_vector_resize(vb_vector_t *, int);


struct vb_vector_namespace {
    int (*len)(vb_vector_t *);
    void (*create)(vb_vector_t *);
    void (*free)(vb_vector_t *);
    void *(*get)(vb_vector_t *, int);
    void (*del)(vb_vector_t *, int);
    void (*add)(vb_vector_t *, void *);
    void (*set)(vb_vector_t *, int, void *);
    void (*resize)(vb_vector_t *, int);
};

static struct vb_vector_namespace
VB_Vector = {
    .len    = &vb_vector_len,
    .create = &vb_vector_create,
    .free   = &vb_vector_free,
    .get    = &vb_vector_get,
    .del    = &vb_vector_del,
    .add    = &vb_vector_add,
    .set    = &vb_vector_set,
    .resize = &vb_vector_resize,
};

#endif
