#ifndef TREE_H
#define TREE_H

#define GTREE_SIZE sizeof(struct _gtree)
#define BTREE_SIZE sizeof(struct _btree)
#define TTREE_SIZE sizeof(struct _ttree)

#define VB_BT_POSTORDER	0
#define VB_BT_PREORDER	1
#define VB_BT_INORDER	2


struct __btree {
    void *info;
    struct __btree *right;
    struct __btree *left;
};

typedef struct __btree * vb_btree_t;


vb_btree_t *vb_btree_create(void *info);
vb_btree_t *vb_btree_insert(vb_btree_t *bt, void *info, int (*compare)(void *, void *));
void vb_btree_visit(vb_btree_t *bt, void (*visit)(void *), int type);
void vb_btree_destroy(vb_btree_t *bt);
size_t vb_btree_count(vb_btree_t *bt);


struct vb__btree_namespace {
    vb_btree_t *(*create)(void *);
    vb_btree_t *(*insert)(vb_btree_t *, void *, int (*)(void *, void *));
    void (*visit)(vb_btree_t *, void (*)(void *), int);
    void (*destroy)(vb_btree_t *);
    size_t (*count)(vb_btree_t *);
};

static struct vb_btree_namespace
VB_Btree = {
    .create  = &vb_btree_create,
    .insert  = &vb_btree_insert,
    .visit   = &vb_btree_visit,
    .destroy = &vb_btree_destroy,
    .count   = &vb_btree_count,
};

#endif
