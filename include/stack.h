#ifndef STACK_H
#define STACK_H

#include "node.h"

#define STACK_SIZE	sizeof(struct _stack)


struct _stack
{
	node_t *head;
	size_t count;
};

typedef struct _stack * stack_t;


stack_t *stack_create();

void *stack_top(stack_t *s);
size_t stack_count(stack_t *s);
bool stack_is_empty(stack_t *s);

void *stack_pop(stack_t *s);
void stack_destroy(stack_t *s);
void stack_push(stack_t *s, void *item);

void stack_visit(stack_t *s, void (*visit)(void *));


struct _stack_namespace
{
    stack_t *(*create)();
    void *(*top)(stack_t *);
    size_t (*count)(stack_t *);
    bool (*is_empty)(stack_t *);
    void *(*pop)(stack_t *);
    void (*destroy)(stack_t *);
    void (*push)(stack_t *, void *);
    void (*visit)(stack_t *, void (*)(void *));
};

static const struct _stack_namespace
Stack = {
    .create   = &stack_create,
    .top      = &stack_top,
    .count    = &stack_count,
    .is_empty = &stack_is_empty,
    .pop      = &stack_pop,
    .destroy  = &stack_destroy,
    .push     = &stack_push,
    .visit    = &stack_visit,
};

#endif
