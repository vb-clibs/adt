#include <stdio.h>
#include <ADT/dlist.h>


void visit(void *elem)
{
    printf("%s\n", elem);
}

int main()
{
    char *str[6] = {
        "a", "b", "c", "d", "e", "f"
    };

    list_t *list = List.create();

    printf("\nINPUT: \n");
    for(int i = 0; i < 6; i++)
    {
        printf(" %s", str[i]);
        List.add_head(list, str[i]);
    }

    printf("\n\nVISIT from head:\n");
    List.visit(list, visit);

    Dlist.destroy(list);
    return 0;
}
