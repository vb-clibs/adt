#include <stdio.h>
#include <ADT/dlist.h>


void visit(void *elem)
{
    printf("%s\n", elem);
}

int main()
{
    char *str[6] = {
        "a", "b", "c", "d", "e", "f"
    };

    dlist_t *list = Dlist.create();

    printf("\nINPUT: \n");
    for(int i = 0; i < 6; i++)
    {
        printf(" %s", str[i]);
        Dlist.add_head(list, str[i]);
    }

    printf("\n\nVISIT from head:\n");
    Dlist.visit_from_head(list, visit);

    printf("\nVISIT from tail:\n");
    Dlist.visit_from_tail(list, visit);

    dnode_t *fh = Dlist.get(list, 4);
    dnode_t *ft = Dlist.rget(list, 4);

    printf("\nGET from head offset: 4\tFOUND: %s\n", fh->info);
    printf("GET from tail offset: 4\tFOUND: %s\n\n", ft->info);

    Dlist.destroy(list);
    return 0;
}
