#include <stdio.h>

#include "ts_queue.h"

int main()
{
	int *tmp[5];

	for(int i = 0; i < 5; i++)
	{
		tmp[i] = malloc(sizeof(int));
		*tmp[i] = i + 1;
		printf("%d ", *tmp[i]);
	}

	TSQueue queue = tsq_create();

	for(int i = 0; i < 5; i++)
		tsq_enqueue(queue, tmp[i]);

	for(int i = 0; i < 5; i++)
	{
		printf("%d\n", *(int *)tsq_dequeue(queue));
	}

	return 0;
}
