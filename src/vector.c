#include <vector.h>


void vector_create(vector_t *v)
{
    v->len = 0;
    v->capacity = sizeof(void *);
    v->items = malloc(sizeof(void *) * v->capacity);
}

int vector_len(vector_t *v)
{
    return v->len;
}

void vector_add(vector_t *v, void *item)
{
    if(v->capacity == v->len)
    {
        vector_resize(v, v->capacity * 2);
    }

    v->items[v->len] = item;
    v->len++;
}

void vector_set(vector_t *v, int index, void *item)
{
    if(index >= 0 && index < v->len)
    {
        v->items[index] = item;
    }
}

void *vector_get(vector_t *v, int index)
{
    if(index >= 0 && index < v->len)
    {
        return v->items[index];
    }

    return NULL;
}

void vector_del(vector_t *v, int index)
{
    if(index < 0 || index >= v->len)
    {
        return;
    }

    v->items[index] = NULL;

    for(int i = index; i < v->len - 1; i++)
    {
        v->items[i] = v->items[i + 1];
        v->items[i + 1] = NULL;
    }

    v->len--;

    if(v->len > 0 && v->len == v->capacity / sizeof(void *))
    {
        vector_resize(v, v->capacity / 2);
    }
}

void vector_free(vector_t *v)
{
    free(v->items);
}

void vector_resize(vector_t *v, int capacity)
{
    void **items = realloc(v->items, sizeof(void *) * capacity);

    if(items != NULL)
    {
        v->items = items;
        v->capacity = capacity;
    }
}
