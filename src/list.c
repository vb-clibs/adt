#include <list.h>


node_t *list_head(list_t *l)
{
    return (l->head);
}

bool list_exist(list_t *l)
{
    return !(l == NULL);
}

bool list_is_empty(list_t *l)
{
    return (l->head->next == NULL);
}

list_t *list_create()
{
	list_t *l = def_malloc(LIST_SIZE);
	l->head = def_malloc(NODE_SIZE);

	l->head->info = NULL;
	l->head->next = NULL;
	l->count = 0;

	return l;
}

void list_destroy(list_t *l)
{
	while(list_delete(l, list_head(l)))
    {
        /* nop */;
    }

	free(l->head);
	free(l);
}

void list_visit(node_t *n, void (*visit)(void *))
{
	if(n->next == NULL)
    {
        return;
    }

	visit(n->next->info);
	list_visit(n->next, visit);
}

void list_add(list_t *l, node_t *node_cur, void* info)
{
	node_t *new_node = def_malloc(NODE_SIZE);
	new_node->info = info;

	new_node->next = node_cur->next;
	node_cur->next = new_node;

	l->count++;
}

bool list_delete(list_t *l, node_t *node_cur)
{
	node_t *temp = NULL;

	if(list_is_empty(l))
    {
		return false;
    }

	temp = node_cur->next;
	node_cur->next = temp->next;

	l->count--;
	free(temp);
	
	return true;
}

node_t *list_search_index(list_t *l, size_t pos)
{
	node_t *temp = list_head(l);

	while(pos-- && temp->next != NULL)
    {
		temp = temp->next;
    }

	return temp;
}

int list_search_item(list_t *l, void* item, int (*compare)(void*, void*))
{
	size_t index = 0;
	node_t *temp = list_head(l);

	while(temp != NULL)
	{
		temp = temp->next;

		if(compare(temp->info, item) == 0)
        {
			return index;
        }

		index++;
	}

	return -1;
}

list_t *list_slice(list_t *l, size_t n)
{
	list_t *list = list_create();
	node_t *temp = list_search_index(l, n);

	list->count = l->count - n;	
	l->count = n;

	list->head->next = temp->next;
	temp->next = NULL;
	
	return list;
}


// XXX TESTME
void list_cat(list_t *l, list_t *m)
{
	node_t *tmp = list_head(l);

	while(tmp != NULL)
    {
		tmp = tmp->next;
    }

	tmp = list_head(m);
}

// XXX TESTME
list_t *list_merge(list_t *l, list_t *m, int (*compare)(void*, void*))
{
	void *tmp_info = NULL;
	node_t *tmp_l = list_head(l);
	node_t *tmp_m = list_head(m);
	list_t *merge = list_create();

	while(tmp_l != NULL && tmp_m != NULL)
    {
		if(compare(tmp_l->info, tmp_m->info) == 1)
		{
			list_add(merge, list_head(merge), tmp_l->info);
			tmp_l = tmp_l->next;
		}
		else
		{
			list_add(merge, list_head(merge), tmp_m->info);
			tmp_m = tmp_m->next;
		}
    }

	while(tmp_l != NULL)
	{
		list_add(merge, list_head(merge), tmp_l->info);
		tmp_l = tmp_l->next;
	}

	while(tmp_m != NULL)
	{
		list_add(merge, list_head(merge), tmp_m->info);
		tmp_m = tmp_m->next;
	}

	return merge;
}

// XXX TESTME
void list_sort(list_t *l, void (*sort)(void* v, size_t n, size_t nb, int (*compare)(void*, void*)), int (*compare)(void*, void*))
{
	void* v = def_malloc(SIZE_INFO * l->count);

	sort(v, l->count, SIZE_INFO, compare);
	free(v);
}

