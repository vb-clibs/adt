#include <dlist.h>


bool dlist_exist(dlist_t *l)
{
	return !(l == NULL);
}

dnode_t *dlist_head(dlist_t *l)
{
	return &l->head;
}

dnode_t *dlist_tail(dlist_t *l)
{
	return &l->tail;
}

bool dlist_is_empty(dlist_t *l)
{
	return (l->head.next == &l->tail || l->tail.prev == &l->head);
}

dlist_t *dlist_create()
{
	dlist_t *l = malloc(DLIST_SIZE);

	l->head.info = NULL;
	l->head.next = &l->tail;
	l->head.prev = &l->tail;

	l->tail.info = NULL;
	l->tail.prev = &l->head;
	l->tail.next = &l->head;

	l->count = 0;

	return l;
}

void dlist_add(dlist_t *l, dnode_t *node_cur, void *info)
{
	dnode_t *new_node = malloc(DNODE_SIZE);
	new_node->info = info;

	new_node->next = node_cur->next;
	new_node->prev = node_cur;

	node_cur->next->prev = new_node;
	node_cur->next = new_node;

	l->count++;
}

void dlist_add_head(dlist_t *l, void *info)
{
    dlist_add(l, dlist_head(l), info);
}

void dlist_add_tail(dlist_t *l, void *info)
{
    dlist_add(l, dlist_tail(l), info);
}

bool dlist_delete(dlist_t *l, dnode_t *node_cur)
{
	dnode_t *temp = node_cur;

	if(dlist_is_empty(l))
	{
		return false;
	}

	temp->next->prev = temp->prev;
	temp->prev->next = temp->next;

	free(temp);
	l->count--;

	return true;
}

void dlist_destroy(dlist_t *l)
{
	dnode_t *temp = dlist_head(l);

	while(!dlist_is_empty(l))
	{
		dlist_delete(l, temp->next);
	}

	free(l);
}

dnode_t *dlist_get_by_offset(dlist_t *l, size_t pos, dnode_t *n, bool dir)
{
	dnode_t *sent = n;

	do
	{
		n = (dir == DL_NEXT) ? n->next : n->prev;
		pos -= (n->info != NULL);
	} while(pos > 0 && n != sent);

	return n;
}

dnode_t *dlist_get(dlist_t *l, size_t pos)
{
    return dlist_get_by_offset(l, pos, dlist_head(l), DL_NEXT);
}

dnode_t *dlist_rget(dlist_t *l, size_t pos)
{
    return dlist_get_by_offset(l, pos, dlist_tail(l), DL_PREV);
}

void dlist_visit(dnode_t *cur, bool dir, void (*visit)(void *))
{
    dnode_t *sent = cur;

    do
    {
        if(cur->info != NULL)
            visit(cur->info);

        cur = (dir == DL_NEXT) ? cur->next : cur->prev;
    } while(cur != sent);
}

void dlist_visit_from_head(dlist_t *l, void (*visit)(void *))
{
    dlist_visit(dlist_head(l), DL_NEXT, visit);
}

void dlist_visit_from_tail(dlist_t *l, void (*visit)(void *))
{
    dlist_visit(dlist_tail(l), DL_PREV, visit);
}

