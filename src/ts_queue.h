#ifndef TS_QUEUE_H_INCLUDED
#define TS_QUEUE_H_INCLUDED

#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>

#define TSQ_SIZE sizeof(struct _queue_safe)


struct _node
{
	void *info;
	struct _node *next;
};

struct _queue_safe
{
	struct _node* head;
	struct _node* tail;

    pthread_mutex_t lock;

    size_t count;
};

typedef struct _queue_safe * TSQueue;


TSQueue tsq_create();

size_t tsq_count(TSQueue q);
bool tsq_isempty(TSQueue q);

void *tsq_dequeue(TSQueue q);
void tsq_destroy(TSQueue q);
void tsq_enqueue(TSQueue q, void *item);

#endif
