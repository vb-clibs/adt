#include <tree.h>


btree_t *btree_create(void *info)
{
	btree_t *bt = malloc(BTREE_SIZE);

	bt->left  = NULL;
	bt->right = NULL;
	bt->info  = info;

	return bt;
}

void btree_destroy(btree_t *bt)
{
	if(bt != NULL)
	{
		btree_destroy(bt->left);
		btree_destroy(bt->right);
		free(bt);
	}
}

size_t btree_count(btree_t *bt)
{
	return (bt != NULL) ? (1 + btree_count(bt->left) + btree_count(bt->right)) : 0;
}


btree_t *btree_insert(btree_t *bt, void* info, int (*compare)(void*, void*))
{
	if(bt == NULL)
    {
		bt = btree_create(info);
    }
	else
    {
		if(bt->info == NULL)
        {
			bt->info = info;
        }
		else
        {
			switch(compare(bt->info, info))
			{
				case -1:
					bt->left  = btree_insert(bt->left,  info, compare);
				break;
				case  1:
					bt->right = btree_insert(bt->right, info, compare);
				break;
			}
        }
	}
    return bt;
}

void btree_visit(btree_t *bt, void (*visit)(void*), btvisit_t type)
{
	if(bt != NULL)
    {
		switch(type)
		{
			case POSTORDER:
				btree_visit(bt->left,  visit, POSTORDER);
				btree_visit(bt->right, visit, POSTORDER);
				visit(bt->info);
			break;

			case PREORDER:
				visit(bt->info);
				btree_visit(bt->left,  visit, PREORDER);
				btree_visit(bt->right, visit, PREORDER);
			break;

			case INORDER:
				btree_visit(bt->left,  visit, INORDER);
				visit(bt->info);
				btree_visit(bt->right, visit, INORDER);
			break;
		}
    }
}

