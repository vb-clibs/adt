#include "ts_queue.h"


size_t tsq_count(TSQueue q)
{
    return q->count;
}

bool tsq_isempty(TSQueue q)
{
    return (q->count == 0);
}

TSQueue tsq_create()
{
        TSQueue q = malloc(sizeof(struct _queue_safe));

        pthread_mutex_init(&q->lock, NULL);

        q->head = NULL;
        q->tail = NULL;
        q->count = 0;

        return q;
}

void tsq_destroy(TSQueue q)
{
        pthread_mutex_lock(&q->lock);

        while(!tsq_isempty(q))
        {
                tsq_dequeue(q);
        }

        free(q);
        pthread_mutex_unlock(&q->lock);
}

void tsq_enqueue(TSQueue q, void *item)
{
        struct _node *new_item = malloc(sizeof(struct _node));
        new_item->info = item;

        pthread_mutex_lock(&q->lock);

        q->count++;

        if(q->tail == NULL)
        {
                q->tail = new_item;
                q->head = q->tail;
        }
        else
        {
                new_item->next = q->tail;
                q->tail = new_item;
        }

        pthread_mutex_unlock(&q->lock);
}

void *tsq_dequeue(TSQueue q)
{
        void *item = NULL;
        struct _node *tmp = q->tail;

        if(tmp != NULL)
        {
                pthread_mutex_lock(&q->lock);

                while(tmp->next != NULL)
                {
                        q->head = tmp;
                        tmp = tmp->next;
                }

                q->count--;
                item = tmp->info;
                free(tmp);

                pthread_mutex_unlock(&q->lock);
        }

        return item;
}
