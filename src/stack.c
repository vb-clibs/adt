#include <stack.h>


void *stack_top(stack_t *s)
{
    return s->head->info;
}

size_t stack_count(stack_t *s)
{
    return s->count;
}

bool stack_is_empty(stack_t *s)
{
    return (s->count == 0);
}

void stack_visit(stack_t *s, PTR_VISIT)
{
	while(!stack_is_empty(s))
    {
        visit(stack_pop(s));        
    }
}

stack_t *stack_create()
{
	stack_t *s = malloc(STACK_SIZE);

	s->head  = NULL;
	s->count = 0;

	return s;
}

void stack_push(stack_t *s, void *item)
{
	node_t *new_node = malloc(NODE_LINEAR);

	new_node->info = item;
	new_node->next = s->head;
	s->head = new_node;

	s->count++;
}

void *stack_pop(stack_t *s)
{
	node_t *temp = s->head->next;
	void *item = s->head->info;

	free(s->head);
	s->head = temp;
	s->count--;

	return item;
}

void stack_destroy(stack_t *s)
{
	while(!stack_is_empty(s))
	{
		stack_pop(s);
	}
	free(s);
}
